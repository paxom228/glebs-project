
import data from './mock.data.js';

export { postsOutput };

function postsOutput() {
  const urlParams = new URLSearchParams(location.search);
  let postId = urlParams.get('post');
  if (!postId) {
    return;
  }
  const post = data.POSTS.find(post => post.id === +postId);
  if (!post) {
    return console.log('not found');
  }
  const user = data.USERS.find(user => user.id === post.userId);
  document.getElementById('top-line-text').innerHTML = post.title;
  document.getElementById('mid-card-text').innerHTML = post.body;
  document.getElementById('top-card').style.backgroundImage=post.postImg;
  document.getElementById('user-img').src=user.userImg;
  document.getElementById('user-name').innerHTML = user.name;
  const tag = data.TAGS.find(tag => tag.id === post.tagId);
  const color = Object.values(tag.color);
  const title = Object.values(tag.title);
  for (let i = 0; i < Object.keys(tag.title).length; i++) {
    let span = document.createElement('span');
    span.className='tag-span';
    span.setAttribute('id', `span-${i}`);
    document.getElementById('mid-card-tag').append(span);
    document.getElementById(`span-${i}`).innerHTML= title[i];
    document.querySelector(`#span-${i}`).style.borderColor= color[i];
    document.querySelector(`#span-${i}`).style.color= color[i];
  }
}
